from flask import Flask, request, jsonify
from tttgame import TicTacToeGame

app = Flask(__name__)
board = TicTacToeGame()

@app.route('/api/move', methods=['POST'])
def move():
    move = request.get_json()
    row, col, token = move['row'], move['col'], move['token']
    if not board.add_token(row, col, token):
        return jsonify({'result': 'error', 'reason': 'Invalid move'}), 400
    else:
        return jsonify({'result': 'ok'}), 201

@app.route('/api/status', methods=['GET'])
def status():
    return jsonify({'board': board.to_json()})

@app.route('/api/winner', methods=['GET'])
def winner():
    has_winner, who = board.check_winner()
    return jsonify({'has_winner': has_winner, 'who': who})

if __name__ == '__main__':
    app.run(debug=False)
