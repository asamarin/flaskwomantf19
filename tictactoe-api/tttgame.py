class TicTacToeGame():
    BOARD_SIZE = 3
    VALID_TOKENS = frozenset(['X', 'O'])

    def __init__(self):
        self.board = [['-' for _ in range(self.BOARD_SIZE)] for _ in range(self.BOARD_SIZE)]

    def add_token(self, row, col, token):
        """ Add a token to the board

        Returns True if parameters are sane and token
        is successfully added; False otherwise
        """
        # Validation
        if not (    0 <= row <= self.BOARD_SIZE - 1
                and 0 <= col <= self.BOARD_SIZE - 1
                and token in self.VALID_TOKENS
                and self.board[row][col] == '-'):
            return False
        # Looks good, set it
        self.board[row][col] = token
        return True

    def to_json(self):
        """ Output current board state in a JSON-friendly structure

        Returns a list of dictionaries, each of them describing every cell
        including its row and col coordinates and the token
        occupying it; a dash '-' represents an empty cell
        """
        out = []
        for i, row in enumerate(self.board):
            for j, token in enumerate(row):
                out.append({'row': i, 'col': j, 'token': token})
        return out

    def check_winner(self):
        """ Check if there's a winner on the current game

        Returns (False, None) if nobody has won yet,
        and (True, <token>) otherwise
        """
        for token in self.VALID_TOKENS:
            has_won = False
            # Rows
            for i in range(self.BOARD_SIZE):
                has_won |= all([cell == token for cell in self.board[i]])
            # Columns
            for i in range(self.BOARD_SIZE):
                col = [self.board[row][i] for row in range(self.BOARD_SIZE)]
                has_won |= all([cell == token for cell in col])
            # Diagonals
            main_diag = [self.board[i][i] for i in range(self.BOARD_SIZE)]
            secondary_diag = [self.board[i][(self.BOARD_SIZE - 1) - i]
                              for i in range(self.BOARD_SIZE)]
            has_won |= all([cell == token for cell in main_diag])
            has_won |= all([cell == token for cell in secondary_diag])
            # Check if this player has won yet
            if has_won:
                return (True, token)
        return (False, None)
