#!/usr/bin/env python

import random
import re
import sys

MAX_RANDOM = 100
MAX_ATTEMPTS = 5

def take_input(prompt):
    while True:
        user_input = input(prompt).strip()
        if not re.match(r'^\d+$', user_input):
            print('Only numbers! ', end='')
            continue
        if not 0 <= int(user_input) <= MAX_RANDOM:
            print(f'Between 0 and {MAX_RANDOM}! ', end='')
            continue
        break
    return int(user_input)

def guess():
    number = random.randint(0, MAX_RANDOM)
    guess = -1
    attempts = MAX_ATTEMPTS
    while (guess != number) and (attempts > 0):
        guess = take_input(f'Your input ({attempts} attempts left): ')
        if guess > number:
            print('Try again with a smaller number')
        elif guess < number:
            print('Try again with a bigger number')
        else:
            break
        attempts -= 1
    if attempts > 0:
        print('You won!')
    else:
        print('You lost!')

if __name__ == '__main__':
    guess()
